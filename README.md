Linux resrouces will be posted here as club leadership runs accross them.  If you have something you think would be helpful for fellow club members, let someone know!

Linux Links:

All things Ubuntu Server: https://help.ubuntu.com/lts/serverguide/serverguide.pdf

Beginner Bash Tutorial: http://linuxcommand.org/

Explains parts of bash commands: https://explainshell.com/

Over the Wire Bandit (Intro to command line challenge): http://overthewire.org/wargames/bandit/

TMUX Cheat Sheet: https://gist.github.com/MohamedAlaa/2961058